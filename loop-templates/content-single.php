<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

// https://wp-events-plugin.com/documentation/placeholders/
// 
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">
<?php if( get_field('place') ) {
	$adr = get_field('place')["address"];
	$adrName = explode(", ",$adr)[0];
} else {
	$adr = "";
	$adrName = "";
} 
?>
<?php if( eo_get_the_start() ): ?>
<script type="application/ld+json">{
	"@context": "http://schema.org",
    "@type": "Event",
    "startDate": "<?php echo eo_get_next_occurrence(eo_the_start($format = 'Y-m-d\TH:i:s-0400')); ?>",
    "endDate": "<?php echo eo_get_next_occurrence(eo_the_end($format = 'Y-m-d\TH:i:s-0400')); ?>",
    "name": "<?php echo the_title(); ?>",
    "url": "<?php echo the_permalink(); ?>",
    "eventStatus": "http://schema.org/EventScheduled",
    "eventAttendanceMode": "http://schema.org/OfflineEventAttendanceMode",
    "location": {
        "@type": "Place",
        	"name": "<?php echo $adrName; ?>",
            "address": {
                "@type": "PostalAddress",
                "streetAddress": "<?php echo $adr; ?>"
            }
        },
        "description": "<?php echo wp_trim_words(get_the_excerpt(), 20, '...'); ?>",
        "image": "<?php echo get_the_post_thumbnail_url(); ?>",
        "performers": []
    }
</script>
<?php endif; ?>

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">

		<?php if( eo_get_the_start() ): ?>
			<p>Next date:
				<strong>
					<?php echo eo_get_next_occurrence( eo_get_event_datetime_format() ); ?>
				</strong></p>
		<?php endif; ?>

		<?php if( get_field('startDate') ): ?>
			<p>Best time to go: 
				<strong>
					<?php the_field('startDate'); ?> - <?php the_field('endDate'); ?>
				</strong></p>
		<?php endif; ?>

		<div class="row justify-content-between mx-0 my-3">
			<?php if( get_field('place') ): $adr = get_field('place')["address"]; ?>
		    <a class="btn btn-dark border d-inline col-9 text-truncate" target="_blank"
		       href="https://www.google.com/maps/dir//<?php echo urlencode($adr); ?>" rel="noopener">
		        <i class="fas fa-map-marked"></i>&nbsp;<?php echo substr($adr, 0, 50); ?>
		    </a>
		    <?php endif; ?>
		    <?php if( get_field('url') ): ?>
		    <a class="d-inline col-3 btn btn-dark border" target="_blank"
		       href="<?php the_field('url'); ?>" rel="noopener">
		        <i class="fas fa-globe-americas"></i> site
		    </a>
		    <?php endif; ?>
		</div>

			<!--php understrap_posted_on();-->

		</div><!-- .entry-meta -->

	</header><!-- .entry-header -->

	<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

	<div class="entry-content">

		<?php the_content(); ?>

		<?php
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
				'after'  => '</div>',
			)
		);
		?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php understrap_entry_footer(); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
